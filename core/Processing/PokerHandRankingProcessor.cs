﻿using System;
using System.Collections.Generic;
using System.Linq;
using core.Abstractions;
using core.Data;

namespace core.Processing
{
	/// <summary>
	/// Class for poker hand rank calculation
	/// </summary>
	public class PokerHandRankingProcessor : ICardHandRankingProcessor
	{
		private const int CountOfCardsInTheHand = 5;

		/// <summary>
		/// Returns best poker hand rank
		/// </summary>
		public ICardHandRank CalculateCardHandRank (ICard[] cards)
		{
			if (cards == null || cards.Length != CountOfCardsInTheHand)
			{
				throw new ArgumentException("Count of cards in the poker hand must be 5.");
			}

			if (cards.Distinct().Count() != CountOfCardsInTheHand)
			{
				throw new ArgumentException("Incorrect hand: duplicate cards in the hand is forbidden.");
			}

			return GetRank(cards);
		}

		/// <summary>
		/// Generates a random poker hand
		/// </summary>
		public ICard[] GenerateRandomHand ()
		{
			List<ICard> cards = new List<ICard>();

			for (int cardIndex = 0; cardIndex < 5; cardIndex++)
			{
				int rankRandom = new Random().Next(0, 13);
				int suitRandom = new Random().Next(0, 3);

				Rank rank = Rank.Create(Rank.GetAll().ElementAt(rankRandom).Code);
				Suit suit = Suit.Create(Suit.GetAll().ElementAt(suitRandom).Code);

				PokerCard card = new PokerCard(rank.Code + suit.Code);

				if (cards.Contains(card))
				{
					cardIndex--;
					continue;
				}

				cards.Add(new PokerCard(rank.Code + suit.Code));
			}

			return cards.ToArray();
		}

		/// <summary>
		/// The most simplified behavior to good code view and debug
		/// </summary>
		private PokerHandRank GetRank (ICard[] cards)
		{
			if (HasRoyalFlush(cards))
			{
				return PokerHandRank.RoyalFlush;
			}

			if (HasStraightFlush(cards))
			{
				return PokerHandRank.StraightFlush;
			}

			if (HasFourOfAKind(cards))
			{
				return PokerHandRank.FourOfAKind;
			}

			if (HasIsFullHouse(cards))
			{
				return PokerHandRank.FullHouse;
			}

			if (HasFlush(cards))
			{
				return PokerHandRank.Flush;
			}

			if (HasStraight(cards))
			{
				return PokerHandRank.Straight;
			}

			if (HasThreeOfAKind(cards))
			{
				return PokerHandRank.ThreeOfAKind;
			}

			if (HasTwoPair(cards))
			{
				return PokerHandRank.TwoPair;
			}

			if (HasPair(cards))
			{
				return PokerHandRank.Pair;
			}

			return PokerHandRank.HighCard;
		}

		private bool HasRoyalFlush (ICard[] cards)
		{
			Rank[] ranks = cards.Select(x => x.Rank).OrderByDescending(x => x.Value).ToArray();
			Suit[] suits = cards.Select(x => x.Suit).OrderBy(x => x.Code).ToArray();

			return ranks[0] == Rank.Ace && ranks.Distinct().Count() == CountOfCardsInTheHand && ranks[0].Value - ranks[4].Value == 4 && suits[0] == suits[4];
		}

		private bool HasStraightFlush (ICard[] cards)
		{
			Rank[] ranks = cards.Select(x => x.Rank).OrderByDescending(x => x.Value).ToArray();
			Suit[] suits = cards.Select(x => x.Suit).OrderBy(x => x.Code).ToArray();

			return ranks.Distinct().Count() == CountOfCardsInTheHand && ranks[0].Value - ranks[4].Value == 4 && suits[0] == suits[4];
		}

		private bool HasFourOfAKind (ICard[] cards)
		{
			Rank[] ranks = cards.Select(x => x.Rank).OrderByDescending(x => x.Value).ToArray();

			return ranks[0].Value == ranks[3].Value || ranks[1].Value == ranks[4].Value;
		}

		private bool HasIsFullHouse (ICard[] cards)
		{
			Rank[] ranks = cards.Select(x => x.Rank).OrderByDescending(x => x.Value).ToArray();

			return (ranks[0].Value == ranks[2].Value && ranks[3].Value == ranks[4].Value)
				|| (ranks[0].Value == ranks[1].Value && ranks[2].Value == ranks[4].Value);
		}

		private bool HasFlush (ICard[] cards)
		{
			Suit[] suits = cards.Select(x => x.Suit).OrderBy(x => x.Code).ToArray();

			return suits[0] == suits[4];
		}

		private bool HasStraight (ICard[] cards)
		{
			Rank[] ranks = cards.Select(x => x.Rank).OrderByDescending(x => x.Value).ToArray();

			return ranks.Distinct().Count() == CountOfCardsInTheHand && ranks[0].Value - ranks[4].Value == 4;
		}

		private bool HasThreeOfAKind (ICard[] cards)
		{
			Rank[] ranks = cards.Select(x => x.Rank).OrderByDescending(x => x.Value).ToArray();
			
			return ranks.Any(t => ranks.Count(x => x.Value == t.Value) == 3);
		}

		private bool HasTwoPair (ICard[] cards)
		{
			Rank[] ranks = cards.Select(x => x.Rank).OrderByDescending(x => x.Value).ToArray();

			bool hasFirstPair = false;

			for (int i = 0; i < ranks.Length; i++)
			{
				if (ranks.Count(x => x.Value == ranks[i].Value) == 2)
				{
					if (!hasFirstPair)
					{
						hasFirstPair = true;
						i++;
					}
					else
					{
						return true;
					}
				}
			}

			return false;
		}

		private bool HasPair (ICard[] cards)
		{
			Rank[] ranks = cards.Select(x => x.Rank).OrderByDescending(x => x.Value).ToArray();

			return ranks.Any(t => ranks.Count(x => x.Value == t.Value) == 2);
		}
	}
}
