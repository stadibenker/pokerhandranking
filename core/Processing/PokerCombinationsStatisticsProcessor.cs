﻿using System;
using System.Collections.Generic;
using System.Linq;
using core.Abstractions;
using core.Data;

namespace core.Processing
{
	/// <summary>
	/// Processor for generate poker hands and calculating hand ranks statistics
	/// </summary>
	public class PokerCombinationsStatisticsProcessor
	{
		private readonly ICardHandRankingProcessor _pokerHandRankingProcessor;

		private Dictionary<ICardHandRank, double> _statistics;

		/// <summary>
		/// Constructor for PokerCombinationsStatisticsProcessor
		/// </summary>
		public PokerCombinationsStatisticsProcessor (ICardHandRankingProcessor pokerHandRankingProcessor)
		{
			_pokerHandRankingProcessor = pokerHandRankingProcessor;
		}

		/// <summary>
		/// Returns combinations (poker hand) statistics for combinationsToGenerateCount hands
		/// </summary>
		public Dictionary<ICardHandRank, double> GetCombinationsStatistics (int combinationsToGenerateCount)
		{
			_statistics = new Dictionary<ICardHandRank, double>();

			int i = 0;

			while (i < combinationsToGenerateCount)
			{
				ICard[] cards = _pokerHandRankingProcessor.GenerateRandomHand();

				ICardHandRank combination = _pokerHandRankingProcessor.CalculateCardHandRank(cards.ToArray());

				if (_statistics.ContainsKey(combination))
				{
					_statistics[combination] ++;
				}
				else
				{
					_statistics.Add(combination, 1);
				}

				i++;
			}
			
			return _statistics;
		}

		/// <summary>
		/// Returns PokerCombinationStatistics
		/// </summary>
		public List<PokerCombinationStatistics> ProcessStatistics (Dictionary<ICardHandRank, double> statistics)
		{
			List<PokerCombinationStatistics> result = new List<PokerCombinationStatistics>();
			
			double totalCombinationsCount = statistics.Values.Sum();
			
			foreach (ICardHandRank cardHandRank in statistics.Keys)
			{
				PokerCombinationStatistics statisticsItem = new PokerCombinationStatistics
				{
					Name = cardHandRank.Name,
					Count = statistics[cardHandRank],
					Percent = Math.Round(statistics[cardHandRank] / totalCombinationsCount * 100, 4)
				};

				result.Add(statisticsItem);
			}

			return result.OrderByDescending(x => x.Count).ToList();
		}
	}
}
