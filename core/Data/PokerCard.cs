﻿using System;
using System.Text.RegularExpressions;
using core.Abstractions;

namespace core.Data
{
	/// <summary>
	/// Class for presentation Poker card data
	/// </summary>
	public class PokerCard : ICard
	{
		private const string RankPattern = @"^([2-9]|10|(J|Q|K|A))";
		private const string SuitPattern = @"(s|h|c|d)$";

		/// <summary>
		/// Card rank
		/// </summary>
		public Rank Rank { get; }
		
		/// <summary>
		/// Card suit
		/// </summary>
		public Suit Suit { get; }

		/// <summary>
		/// Constructor for Poker card. Initializes a Poker card with rank and suit
		/// </summary>
		public PokerCard (string cardDescription)
		{
			if (!Regex.IsMatch(cardDescription, RankPattern + SuitPattern))
			{
				throw new ArgumentException($"Invalid Poker card description format '{cardDescription}'");
			}

			Rank = Rank.Create(Regex.Match(cardDescription, RankPattern).Value);
			Suit = Suit.Create(Regex.Match(cardDescription, SuitPattern).Value);
		}

		/// <summary>
		/// Returns Card rank and suit
		/// </summary>
		public override string ToString ()
		{
			return $"{Rank} {Suit}";
		}

		/// <summary>
		/// Returns basic Card description
		/// </summary>
		public string GetDescription ()
		{
			return Rank.Code + Suit.Code;
		}

		public override bool Equals (object obj)
		{
			if (obj == null || GetType() != obj.GetType()) return false;
			
			PokerCard card = (PokerCard) obj;

			return Rank.Code == card.Rank.Code && Suit.Code == card.Suit.Code;
		}

		public override int GetHashCode ()
		{
			unchecked
			{
				return ((Rank != null ? Rank.GetHashCode() : 0) * 397) ^ (Suit != null ? Suit.GetHashCode() : 0);
			}
		}
	}
}
