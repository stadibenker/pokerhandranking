﻿using core.Abstractions;

namespace core.Data
{
	/// <summary>
	/// Class for presentation poker combination statistics data
	/// </summary>
	public class PokerCombinationStatistics : ICombinationStatistics
	{
		/// <summary>
		/// Combination name
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// Count of combination appearances
		/// </summary>
		public double Count { get; set; }

		/// <summary>
		/// Combination probability
		/// </summary>
		public double Percent { get; set; }

		/// <summary>
		/// Returns a humanizing string presentation of PokerCombinationStatistics
		/// </summary>
		public override string ToString ()
		{
			return $"{Name}: appearances count: {Count}, probability: {Percent}%";
		}
	}
}
