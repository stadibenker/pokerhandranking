﻿using core.Abstractions;

namespace core.Data
{
	/// <summary>
	/// Enum for presentation poker hand ranks
	/// </summary>
	public class PokerHandRank : BaseEnumeration<int, PokerHandRank>, ICardHandRank
	{
		/// <summary>
		/// Royal flush
		/// </summary>
		public static readonly PokerHandRank RoyalFlush = new PokerHandRank(10, "Royal flush", "Royal flush");

		/// <summary>
		/// Straight flush
		/// </summary>
		public static readonly PokerHandRank StraightFlush = new PokerHandRank(9, "Straight flush", "Straight flush");

		/// <summary>
		/// Four of a kind
		/// </summary>
		public static readonly PokerHandRank FourOfAKind = new PokerHandRank(8, "Four of a kind", "Four of a kind");

		/// <summary>
		/// Full house
		/// </summary>
		public static readonly PokerHandRank FullHouse = new PokerHandRank(7, "Full house", "Full house");

		/// <summary>
		/// Flush
		/// </summary>
		public static readonly PokerHandRank Flush = new PokerHandRank(6, "Flush", "Flush");

		/// <summary>
		/// Straight
		/// </summary>
		public static readonly PokerHandRank Straight = new PokerHandRank(5, "Straight", "Straight");

		/// <summary>
		/// Three of a kind
		/// </summary>
		public static readonly PokerHandRank ThreeOfAKind = new PokerHandRank(4, "Three of a kind", "Three of a kind");

		/// <summary>
		/// Two pair
		/// </summary>
		public static readonly PokerHandRank TwoPair = new PokerHandRank(3, "Two pair", "Two pair");

		/// <summary>
		/// Pair
		/// </summary>
		public static readonly PokerHandRank Pair = new PokerHandRank(2, "Pair", "Pair");

		/// <summary>
		/// High Card
		/// </summary>
		public static readonly PokerHandRank HighCard = new PokerHandRank(1, "High card", "High card");

		/// <summary>
		/// Rank name
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// Constructor for Poker hand rank item. Initializes item with code, name and description
		/// </summary>
		private PokerHandRank (int code, string name, string description)
			: base(code, description)
		{
			Name = name;
		}
	}
}
