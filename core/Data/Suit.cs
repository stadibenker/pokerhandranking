﻿using core.Abstractions;

namespace core.Data
{
	/// <summary>
	/// Enum for presentation <see cref="Card"/> Suit
	/// </summary>
	public class Suit : BaseEnumeration <string, Suit>
	{
		/// <summary>
		/// Spade
		/// </summary>
		public static readonly Suit Spade = new Suit("s", "Spade");

		/// <summary>
		/// Heart
		/// </summary>
		public static readonly Suit Heart = new Suit("h", "Heart");

		/// <summary>
		/// Clover
		/// </summary>
		public static readonly Suit Clover = new Suit("c", "Clover");

		/// <summary>
		/// Diamond
		/// </summary>
		public static readonly Suit Diamond = new Suit("d", "Diamond");

		/// <summary>
		/// Constructor for Suit item. Initializes Suit with code and description
		/// </summary>
		private Suit (string code, string description) :
			base(code, description) 
		{ }
	}
}
