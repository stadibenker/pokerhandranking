﻿using core.Abstractions;

namespace core.Data
{
	/// <summary>
	/// Enum for presentation <see cref="PokerCard"/> rank
	/// </summary>
	public class Rank : BaseEnumeration <string, Rank>
	{
		/// <summary>
		/// Two
		/// </summary>
		public static readonly Rank Two = new Rank(2, "2", "Two");

		/// <summary>
		/// Three
		/// </summary>
		public static readonly Rank Three = new Rank(3, "3", "Three");

		/// <summary>
		/// Four
		/// </summary>
		public static readonly Rank Four = new Rank(4, "4", "Four");

		/// <summary>
		/// Five
		/// </summary>
		public static readonly Rank Five = new Rank(5, "5", "Five");

		/// <summary>
		/// Six
		/// </summary>
		public static readonly Rank Six = new Rank(6, "6", "Six");

		/// <summary>
		/// Seven
		/// </summary>
		public static readonly Rank Seven = new Rank(7, "7", "Seven");

		/// <summary>
		/// Eight
		/// </summary>
		public static readonly Rank Eight = new Rank(8, "8", "Eight");

		/// <summary>
		/// Nine
		/// </summary>
		public static readonly Rank Nine = new Rank(9, "9", "Nine");

		/// <summary>
		/// Ten
		/// </summary>
		public static readonly Rank Ten = new Rank(10, "10", "Ten");

		/// <summary>
		/// Jack
		/// </summary>
		public static readonly Rank Jack = new Rank(11, "J", "Jack");

		/// <summary>
		/// Queen
		/// </summary>
		public static readonly Rank Queen = new Rank(12, "Q", "Queen");

		/// <summary>
		/// King
		/// </summary>
		public static readonly Rank King = new Rank(13, "K", "King");

		/// <summary>
		/// Ace
		/// </summary>
		public static readonly Rank Ace = new Rank(14, "A", "Ace");

		/// <summary>
		/// Rank value
		/// </summary>
		public int Value { get; }

		/// <summary>
		/// Constructor for Rank item. Initializes Rank with value and description
		/// </summary>
		private Rank (int value, string code, string description) :
			base(code, description)
		{
			Value = value;
		}
	}
}
