﻿namespace core.Abstractions
{
	/// <summary>
	/// Combination statistics
	/// </summary>
	public interface ICombinationStatistics
	{
		/// <summary>
		/// Combination name
		/// </summary>
		string Name { get; set; }

		/// <summary>
		/// Count of combination appearances
		/// </summary>
		double Count { get; set; }

		/// <summary>
		/// Combination appearances percentage
		/// </summary>
		double Percent { get; set; }
		
	}
}
