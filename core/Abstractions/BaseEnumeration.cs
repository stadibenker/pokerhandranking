﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace core.Abstractions
{
	/// <summary>
	/// Base class for enum operations
	/// </summary>
	public abstract class BaseEnumeration <TCode, TEnumType> where TEnumType : BaseEnumeration<TCode, TEnumType>
	{
		/// <summary>
		/// Enum item code
		/// </summary>
		public TCode Code { get; set; }

		/// <summary>
		/// Enum item description
		/// </summary>
		public string Description { get; set; }
		
		/// <summary>
		/// Constructor for base item
		/// </summary>
		protected BaseEnumeration (TCode code, string description)
		{
			Code = code;
			Description = description;
		}

		private static readonly ConcurrentDictionary<TCode, TEnumType> AllTypes = new ConcurrentDictionary<TCode, TEnumType>(GetAll().ToDictionary(key => key.Code, val => val));

		/// <summary>
		/// Returns description
		/// </summary>
		public override string ToString ()
		{
			return Description;
		}

		/// <summary>
		/// Return all values of enumeration
		/// </summary>
		public static IEnumerable<TEnumType> GetAll ()
		{
			var fields = typeof(TEnumType).GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.DeclaredOnly);

			return fields.Select(f => f.GetValue(null)).Cast<TEnumType>();
		}

		/// <summary>
		/// Creates enum instance from its value representation
		/// </summary>
		public static TEnumType Create (TCode code)
		{
			if (!AllTypes.ContainsKey(code))
			{
				throw new ArgumentException($"Not supported code provided: {code}");
			}

			return AllTypes[code];
		}
	}
}
