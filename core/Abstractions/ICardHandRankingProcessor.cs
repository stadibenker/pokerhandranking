﻿namespace core.Abstractions
{
	/// <summary>
	/// Interface to declaration of methods for card rank calculation
	/// </summary>
	public interface ICardHandRankingProcessor
	{
		/// <summary>
		/// Declaration of method to card hand rank calculation
		/// </summary>
		ICardHandRank CalculateCardHandRank (ICard[] cards);

		/// <summary>
		/// Generates a random hand
		/// </summary>
		ICard[] GenerateRandomHand ();
	}
}
