﻿using core.Data;

namespace core.Abstractions
{
	/// <summary>
	/// Card properties
	/// </summary>
	public interface ICard
	{
		/// <summary>
		/// Card rank
		/// </summary>
		Rank Rank { get; }

		/// <summary>
		/// Card suit
		/// </summary>
		Suit Suit { get; }
	}
}
