﻿namespace core.Abstractions
{
	/// <summary>
	/// Card hand rank
	/// </summary>
	public interface ICardHandRank
	{
		string Name { get; set; }
	}
}
