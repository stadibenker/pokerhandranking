﻿using System;
using System.Collections.Generic;
using core.Abstractions;
using core.Data;
using core.Processing;
using Microsoft.Extensions.DependencyInjection;

namespace PokerHandRanking
{
	/// <summary>
	/// Main class for user working with the program
	/// </summary>
	public class UserDialog
	{
		private readonly IServiceProvider _serviceProvider;

		/// <summary>
		/// Constructor for UserDialog
		/// </summary>
		public UserDialog (IServiceProvider serviceProvider)
		{
			_serviceProvider = serviceProvider;
		}

		/// <summary>
		/// Starts the user dialog
		/// </summary>
		public void Start()
		{
			Console.WriteLine("Poker hand ranking tool\n");
			try
			{
				while (true)
				{
					int choice = GetUserChoice();

					switch (choice)
					{
						case 1:
							UserInputCombination();
							break;
						case 2:
							GenerateCombinations();
							break;
						case 3: return;
						default: throw new ArgumentException($"Undefined choice: {choice}");
					}
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine($"An exception occurred: {ex.Message}");
			}
		}

		private int GetUserChoice()
		{
			Console.WriteLine("1. Manual combination entering");
			Console.WriteLine("2. Automatically combination generation");
			Console.WriteLine("3. Exit");

			while (true)
			{
				try
				{
					int.TryParse(Console.ReadLine(), out int choice);

					if (choice == 1 || choice == 2 || choice == 3)
					{
						return choice;
					}

					Console.WriteLine("Incorrect input. Please choose between 1 and 2");
				}
				catch (Exception)
				{
					Console.WriteLine("Input format is incorrect. Please, try again.");
				}
			}
		}

		private void UserInputCombination ()
		{
			Console.Clear();

			Console.WriteLine(Manual.GetManual());

			while (true)
			{
				List<ICard> cards = new List<ICard>();

				for (int i = 0; i < 5; i++)
				{
					Console.Write($"Enter [{i + 1}] card: ");
					try
					{
						cards.Add(new PokerCard(Console.ReadLine()));
					}
					catch (ArgumentException ex)
					{
						Console.WriteLine(ex.Message);
						i--;
					}
				}

				try
				{
					ICardHandRankingProcessor pokerCardHandRankingProcessor = _serviceProvider.GetRequiredService<ICardHandRankingProcessor>();
					
					ICardHandRank rank = pokerCardHandRankingProcessor.CalculateCardHandRank(cards.ToArray());

					Console.WriteLine($"Entered combination is {rank.Name}");
				}
				catch (ArgumentException ex)
				{
					Console.WriteLine(ex.Message);
				}

				Console.WriteLine("Would you like to enter another combination? (y/n)");

				string choice;
				do
				{
					choice = Console.ReadLine();

					switch (choice)
					{
						case "y": break;
						case "n": return;
						default:
							Console.WriteLine("Undefined command. Please, try again.");
							continue;
					}
				} while (!string.Equals(choice, "y"));
			}
		}

		private void GenerateCombinations()
		{
			Console.Clear();

			Console.WriteLine("Enter count of combinations to generate (recommended is 100 000 - 200 000)");

			while (true)
			{
				int combinationsCount = 0;

				Console.Write(">:");

				try
				{
					int.TryParse(Console.ReadLine(), out combinationsCount);
				}
				catch (Exception)
				{
					Console.WriteLine("Incorrect input. Please, try again.");
				}

				if (combinationsCount <= 0)
				{
					Console.WriteLine("Please enter more than 1 combinations to generate");
					continue;
				}

				Console.WriteLine();

				PokerCombinationsStatisticsProcessor statisticsProcessor = _serviceProvider.GetRequiredService<PokerCombinationsStatisticsProcessor>();

				Dictionary<ICardHandRank, double> statistics = statisticsProcessor.GetCombinationsStatistics(combinationsCount);

				List<PokerCombinationStatistics> result = statisticsProcessor.ProcessStatistics(statistics);

				result.ForEach(mes => Console.WriteLine(mes.ToString()));

				Console.WriteLine("Would you like to continue? (y/n)");

				string choice;
				do
				{
					choice = Console.ReadLine();

					switch (choice)
					{
						case "y": break;
						case "n": return;
						default:
							Console.WriteLine("Undefined command. Please, try again.");
							continue;
					}
				} while (!string.Equals(choice, "y"));
			}
		}
	}
}