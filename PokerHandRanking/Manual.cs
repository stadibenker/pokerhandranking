﻿namespace PokerHandRanking
{
	/// <summary>
	/// Class for store the manual
	/// </summary>
	public static class Manual
	{
		/// <summary>
		/// Returns manual
		/// </summary>
		public static string GetManual ()
		{
			return @"Every card is a string containing the card value (with the upper-case initial for face-cards) and the lower-case initial for suits.
	Card naming conversion:
	- Ranks:
		Two - 2
		Three - 3
		...
		Ten - 10
		Jack - J
		Queen - Q
		King - K
		Ace - A
	- Suits:
		Spade - s
		Heart - h
		Clover - c
		Diamond - d
	Examples:
		2s = Two of spades
		10d = Ten of diamonds
		Jc = Jack of clovers
		Ah = Ace of hearts";
		}
	}
}
