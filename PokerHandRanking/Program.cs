﻿using System;
using core.Abstractions;
using core.Processing;
using Microsoft.Extensions.DependencyInjection;

namespace PokerHandRanking
{
	class Program
	{
		static void Main (string[] args)
		{
			try
			{
				IServiceProvider provider = ConfigureServices();

				UserDialog dialog = provider.GetRequiredService<UserDialog>();

				dialog.Start();
			}
			catch (Exception ex)
			{
				Console.WriteLine($"An error occurred while start the program: {ex.Message}");
			}
		}
		
		private static IServiceProvider ConfigureServices ()
		{
			IServiceProvider serviceProvider = new ServiceCollection()
				.AddTransient<UserDialog>()
				.AddTransient<ICardHandRankingProcessor, PokerHandRankingProcessor>()
				.AddTransient<PokerCombinationsStatisticsProcessor>()
				.BuildServiceProvider();

			return serviceProvider;
		}
	}
}
