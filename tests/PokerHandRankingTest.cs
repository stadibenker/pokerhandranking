﻿using System;
using core.Abstractions;
using core.Data;
using core.Processing;
using NUnit.Framework;

namespace tests
{
	public class PokerHandRankingTest
	{
		private const string HighCard = "High card";
		private const string Pair = "Pair";
		private const string TwoPair = "Two pair";
		private const string ThreeOfAKind = "Three of a kind";
		private const string Straight = "Straight";
		private const string Flush = "Flush";
		private const string FullHouse = "Full house";
		private const string FourOfAKind = "Four of a kind";
		private const string StraightFlush = "Straight flush";
		private const string RoyalFlush = "Royal flush";
		
		private static ICard[] GetCardsSet (string combinationName)
		{
			switch (combinationName)
			{
				case HighCard: return new ICard[] {new PokerCard("2d"), new PokerCard("5d"), new PokerCard("6s"), new PokerCard("Kh"), new PokerCard("8c")};
				case Pair: return new ICard[] { new PokerCard("2d"), new PokerCard("2h"), new PokerCard("6s"), new PokerCard("Kh"), new PokerCard("8c") };
				case TwoPair: return new ICard[] { new PokerCard("2d"), new PokerCard("5d"), new PokerCard("6s"), new PokerCard("6h"), new PokerCard("2s") };
				case ThreeOfAKind: return new ICard[] { new PokerCard("2d"), new PokerCard("5d"), new PokerCard("5s"), new PokerCard("5h"), new PokerCard("Ac") };
				case Straight: return new ICard[] { new PokerCard("2d"), new PokerCard("3d"), new PokerCard("4s"), new PokerCard("5h"), new PokerCard("6c") };
				case Flush: return new ICard[] { new PokerCard("2d"), new PokerCard("5d"), new PokerCard("6d"), new PokerCard("Kd"), new PokerCard("8d") };
				case FullHouse: return new ICard[] { new PokerCard("2d"), new PokerCard("2h"), new PokerCard("6s"), new PokerCard("6h"), new PokerCard("6d") };
				case FourOfAKind: return new ICard[] { new PokerCard("Kd"), new PokerCard("Kh"), new PokerCard("Ks"), new PokerCard("Kc"), new PokerCard("8c") };
				case StraightFlush: return new ICard[] { new PokerCard("3d"), new PokerCard("4d"), new PokerCard("5d"), new PokerCard("6d"), new PokerCard("7d") };
				case RoyalFlush: return new ICard[] { new PokerCard("10c"), new PokerCard("Jc"), new PokerCard("Qc"), new PokerCard("Kc"), new PokerCard("Ac") };
				default:throw new ArgumentException($"Combination '{combinationName}' does not supported.");
			}
		}

		[TestCase(HighCard)]
		[TestCase(Pair)]
		[TestCase(TwoPair)]
		[TestCase(ThreeOfAKind)]
		[TestCase(Straight)]
		[TestCase(Flush)]
		[TestCase(FullHouse)]
		[TestCase(FourOfAKind)]
		[TestCase(StraightFlush)]
		[TestCase(RoyalFlush)]
		public void TestCombinations (string combinationName)
		{
			ICard[] cards = GetCardsSet(combinationName);

			PokerHandRankingProcessor calculator = new PokerHandRankingProcessor();

			string combination = calculator.CalculateCardHandRank(cards).Name;

			Assert.AreEqual(combinationName, combination);
		}
	}
}
