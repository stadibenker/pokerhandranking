using System;
using System.Collections.Generic;
using System.Linq;
using core.Data;
using NUnit.Framework;

namespace tests
{
	public class CardTest
	{
		[Test]
		public void CardInitializationTest ()
		{
			List<Suit> suits = Suit.GetAll().ToList();
			List<Rank> ranks = Rank.GetAll().ToList();

			foreach (Suit suit in suits)
			{
				foreach (Rank rank in ranks)
				{
					PokerCard card = new PokerCard(rank.Code + suit.Code);

					Console.WriteLine(card.ToString());

					Assert.AreEqual(rank.Code + suit.Code, card.GetDescription());
				}
			}
		}
	}
}